<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use OpenBoleto\Banco\BancoDoBrasil;
use OpenBoleto\Banco\Bradesco;
use OpenBoleto\Banco\Hsbc;
use OpenBoleto\Banco\Itau;
use OpenBoleto\Banco\Santander;
use OpenBoleto\Banco\Caixa;
use OpenBoleto\Banco\Brb;
use OpenBoleto\Banco\Unicred;
use OpenBoleto\Banco\Safra;
use OpenBoleto\Agente;
use DateTime;

class BoletoController extends Controller
{
    

	function getBoletoBB()
	{
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		
		$boleto = new BancoDoBrasil(array(
	    // Parâmetros obrigatórios     
		'dataVencimento' => new DateTime('2013-01-24'),
	    'valor' => 23.00,
	    'sequencial' => 1234567, // Para gerar o nosso número     
	    'sacado' => $sacado,
	    'cedente' => $cedente,
	    'agencia' => 1724, // Até 4 dígitos     
	    'carteira' => 18,
	    'conta' => 10403005, // Até 8 dígitos     
	    'convenio' => 1234 // 4, 6 ou 7 dígitos 
	    ));
		
		return $boleto->getOutput();
	}


	function getBoletoBradesco()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Bradesco(array(
		    // Parâmetros obrigatórios
		    'dataVencimento' => new DateTime('2013-01-24'),
		    'valor' => 23.00,
		    'sequencial' => 75896452, // Até 11 dígitos
		    'sacado' => $sacado,
		    'cedente' => $cedente,
		    'agencia' => 1172, // Até 4 dígitos
		    'carteira' => 6, // 3, 6 ou 9
		    'conta' => 0403005, // Até 7 dígitos

		    // Parâmetros recomendáveis
		    //'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
		    'contaDv' => 2,
		    'agenciaDv' => 1,
		    'descricaoDemonstrativo' => array( // Até 5
		        'Compra de materiais cosméticos',
		        'Compra de alicate',
		    ),
		    'instrucoes' => array( // Até 8
		        'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
		        'Não receber após o vencimento.',
		    ),

		    
		));

		echo $boleto->getOutput();
	}
	
	
	function getBoletoHsbc()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Hsbc(array(
		    // Parâmetros obrigatórios
		    'dataVencimento' => new DateTime('2013-01-24'),
		    'valor' => 23.00,
		    'sequencial' => 7589645222, // Até 13 dígitos
		    'sacado' => $sacado,
		    'cedente' => $cedente,
		    'agencia' => 1172, // Até 4 dígitos
		    'carteira' => 'CNR', // CNR
		    'conta' => 0403005, // Até 7 dígitos

		    // Parâmetros recomendáveis
		    //'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
		    'contaDv' => 2,
		    'agenciaDv' => 1,
		    'descricaoDemonstrativo' => array( // Até 5
		        'Compra de materiais cosméticos',
		        'Compra de alicate',
		    ),
		    'instrucoes' => array( // Até 8
		        'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
		        'Não receber após o vencimento.',
		    ),

		    
		));

		echo $boleto->getOutput();
	}
		
	function getBoletoItau()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Itau(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-01-24'),
			'valor' => 23.00,
			'sequencial' => 12345678, // 8 dígitos
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 1724, // 4 dígitos
			'carteira' => 112, // 3 dígitos
			'conta' => 12345, // 5 dígitos
    
			// Parâmetro obrigatório somente se a carteira for
			// 107, 122, 142, 143, 196 ou 198
			'codigoCliente' => 12345, // 5 dígitos
			'numeroDocumento' => 1234567, // 7 dígitos

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			'contaDv' => 2,
			'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),
			
			
		));
		
		echo $boleto->getOutput();
	}
	
	function getBoletoSantander()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Santander(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-01-24'),
			'valor' => 23.00,
			'sequencial' => 12345678901, // Até 13 dígitos
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 1234, // Até 4 dígitos
			'carteira' => 102, // 101, 102 ou 201
			'conta' => 1234567, // Código do cedente: Até 7 dígitos
			 // IOS – Seguradoras (Se 7% informar 7. Limitado a 9%)
			 // Demais clientes usar 0 (zero)
			'ios' => '0', // Apenas para o Santander

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			'contaDv' => 2,
			'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),

			// Parâmetros opcionais
			//'resourcePath' => '../resources',
			//'moeda' => Santander::MOEDA_REAL,
			//'dataDocumento' => new DateTime(),
			//'dataProcessamento' => new DateTime(),
			//'contraApresentacao' => true,
			//'pagamentoMinimo' => 23.00,
			//'aceite' => 'N',
			//'especieDoc' => 'ABC',
			//'numeroDocumento' => '123.456.789',
			//'usoBanco' => 'Uso banco',
			//'layout' => 'layout.phtml',
			//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
			//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
			//'descontosAbatimentos' => 123.12,
			//'moraMulta' => 123.12,
			//'outrasDeducoes' => 123.12,
			//'outrosAcrescimos' => 123.12,
			//'valorCobrado' => 123.12,
			//'valorUnitario' => 123.12,
			//'quantidade' => 1,
		));

		echo $boleto->getOutput();

	}
	
	function getBoletoCaixa()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Caixa(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-01-24'),
			'valor' => 23.00,
			'sequencial' => 00007589226450872, // Até 17 dígitos
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 1172, // Até 4 dígitos
			'carteira' => 'SR', 
			'conta' => 04030, // Até 5 dígitos
			 // IOS – Seguradoras (Se 7% informar 7. Limitado a 9%)
			 // Demais clientes usar 0 (zero)
			'ios' => '0', // Apenas para o Santander

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			'contaDv' => 2,
			'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),

			// Parâmetros opcionais
			//'resourcePath' => '../resources',
			//'moeda' => Santander::MOEDA_REAL,
			//'dataDocumento' => new DateTime(),
			//'dataProcessamento' => new DateTime(),
			//'contraApresentacao' => true,
			//'pagamentoMinimo' => 23.00,
			//'aceite' => 'N',
			//'especieDoc' => 'ABC',
			//'numeroDocumento' => '123.456.789',
			//'usoBanco' => 'Uso banco',
			//'layout' => 'layout.phtml',
			//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
			//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
			//'descontosAbatimentos' => 123.12,
			//'moraMulta' => 123.12,
			//'outrasDeducoes' => 123.12,
			//'outrosAcrescimos' => 123.12,
			//'valorCobrado' => 123.12,
			//'valorUnitario' => 123.12,
			//'quantidade' => 1,
		));

		echo $boleto->getOutput();

	}
	
	function getBoletoBrb()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Brb(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-01-24'),
			'valor' => 23.00,
			'sequencial' => 758964, // Até 6 dígitos
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 172, // Até 3 dígitos
			'carteira' => 1, // 1 ou 2
			'conta' => 0403005, // Até 7 dígitos

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			'contaDv' => 2,
			'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),

			// Parâmetros opcionais
			//'resourcePath' => '../resources',
			//'moeda' => Brb::MOEDA_REAL,
			//'dataDocumento' => new DateTime(),
			//'dataProcessamento' => new DateTime(),
			//'contraApresentacao' => true,
			//'pagamentoMinimo' => 23.00,
			//'aceite' => 'N',
			//'especieDoc' => 'ABC',
			//'numeroDocumento' => '123.456.789',
			//'usoBanco' => 'Uso banco',
			//'layout' => 'layout.phtml',
			//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
			//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
			//'descontosAbatimentos' => 123.12,
			//'moraMulta' => 123.12,
			//'outrasDeducoes' => 123.12,
			//'outrosAcrescimos' => 123.12,
			//'valorCobrado' => 123.12,
			//'valorUnitario' => 123.12,
			//'quantidade' => 1,
		));

		echo $boleto->getOutput();

	}
	
	function getBoletoUnicred()
	{
		
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Unicred(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-07-20'),
			'valor' => 1093.79,
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 3302, // Até 4 dígitos
			'carteira' => 51, // 11, 21, 31, 41 ou 51
			'conta' => 2259, // Até 10 dígitos
			'sequencial' => '13951', // Até 10 dígitos

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			// 'contaDv' => 2,
			// 'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),

			// Parâmetros opcionais
			//'resourcePath' => '../resources',
			//'moeda' => BancoDoBrasil::MOEDA_REAL,
			//'dataDocumento' => new DateTime(),
			//'dataProcessamento' => new DateTime(),
			//'contraApresentacao' => true,
			//'pagamentoMinimo' => 23.00,
			//'aceite' => 'N',
			//'especieDoc' => 'ABC',
			//'numeroDocumento' => '123.456.789',
			//'usoBanco' => 'Uso banco',
			//'layout' => 'layout.phtml',
			//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
			//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
			//'descontosAbatimentos' => 123.12,
			//'moraMulta' => 123.12,
			//'outrasDeducoes' => 123.12,
			//'outrosAcrescimos' => 123.12,
			//'valorCobrado' => 123.12,
			//'valorUnitario' => 123.12,
			//'quantidade' => 1,
		));

		echo $boleto->getOutput();


	}
	
	
	function getBoletoSafra()
	{
		$sacado = new Agente('Fernando Maia', '023.434.234-34', 'ABC 302 Bloco N', '72000-000', 'Brasília', 'DF');
		$cedente = new Agente('Empresa de cosméticos LTDA', '02.123.123/0001-11', 'CLS 403 Lj 23', '71000-000', 'Brasília', 'DF');

		$boleto = new Safra(array(
			// Parâmetros obrigatórios
			'dataVencimento' => new DateTime('2013-01-24'),
			'valor' => 23.00,
			'sequencial' => 75896452, // Até 11 dígitos
			'sacado' => $sacado,
			'cedente' => $cedente,
			'agencia' => 1172, // Até 4 dígitos
			'carteira' => 6, //6
			'conta' => 0403005, // Até 7 dígitos

			// Parâmetros recomendáveis
			//'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
			'contaDv' => 2,
			'agenciaDv' => 1,
			'descricaoDemonstrativo' => array( // Até 5
				'Compra de materiais cosméticos',
				'Compra de alicate',
			),
			'instrucoes' => array( // Até 8
				'Após o dia 30/11 cobrar 2% de mora e 1% de juros ao dia.',
				'Não receber após o vencimento.',
			),

			// Parâmetros opcionais
			//'resourcePath' => '../resources',
			//'cip' => '000', // Apenas para o Safra
			//'moeda' => Safra::MOEDA_REAL,
			//'dataDocumento' => new DateTime(),
			//'dataProcessamento' => new DateTime(),
			//'contraApresentacao' => true,
			//'pagamentoMinimo' => 23.00,
			//'aceite' => 'N',
			//'especieDoc' => 'ABC',
			//'numeroDocumento' => '123.456.789',
			//'usoBanco' => 'Uso banco',
			//'layout' => 'layout.phtml',
			//'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
			//'sacadorAvalista' => new Agente('Antônio da Silva', '02.123.123/0001-11'),
			//'descontosAbatimentos' => 123.12,
			//'moraMulta' => 123.12,
			//'outrasDeducoes' => 123.12,
			//'outrosAcrescimos' => 123.12,
			//'valorCobrado' => 123.12,
			//'valorUnitario' => 123.12,
			//'quantidade' => 1,
		));

		echo $boleto->getOutput();
		
	}
	
	
	
	
	



}
