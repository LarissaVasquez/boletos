<?php

namespace Tests\OpenBoleto\Banco;
use OpenBoleto\Banco\Caixa;

class CaixaTest extends \PHPUnit_Framework_TestCase
{
    public function testInstantiateWithoutArgumentsShouldWork()
    {
        $this->assertInstanceOf('OpenBoleto\\Banco\\Caixa', new Caixa());
    }

    public function testInstantiateShouldWork()
    {
        $instance = new Caixa(array(
            // Parâmetros obrigatórios
            'dataVencimento' => new \DateTime('2013-01-01'),
            'valor' => 10.50,
            'sequencial' => 00123456788870009, // Até 17 dígitos
            'agencia' => 1172, // Até 4 dígitos
            'carteira' => 'SR', 
            'conta' => 40300, // Até 5 dígitos
        ));

        $this->assertInstanceOf('OpenBoleto\\Banco\\Caixa', $instance);
        $this->assertEquals('23791.17209 60012.345678 89040.300504 8 55650000001050', $instance->getLinhaDigitavel());
        $this->assertSame('123456789', (string) $instance->getNossoNumero());
    }
}
