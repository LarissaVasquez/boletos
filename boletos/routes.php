<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/listaboletos',function(){
    return view('listaboletos');
});

Route::get('boleto/brasil', 'BoletoController@getBoletoBB');
Route::get('boleto/bradesco', 'BoletoController@getBoletoBradesco');
Route::get('boleto/hsbc', 'BoletoController@getBoletoHsbc');
Route::get('boleto/itau', 'BoletoController@getBoletoItau');
Route::get('boleto/santander', 'BoletoController@getBoletoSantander');
Route::get('boleto/caixa', 'BoletoController@getBoletoCaixa');
Route::get('boleto/brb', 'BoletoController@getBoletoBrb');
Route::get('boleto/unicred', 'BoletoController@getBoletoUnicred');
Route::get('boleto/safra', 'BoletoController@getBoletoSafra');
Route::auth();

Route::get('/home', 'HomeController@index');
